from django.urls import path

from . import views # views functions

#url mappings
urlpatterns =[
    path('', views.home, name='home'),
    path('add', views.add, name='add')
]