from django.db import models

# Create your models here.

class Destination(models.Model): # convert this class into a Model
    #no need to declare id as field here - the orm auto created the pk for tables
    name = models.CharField(max_length=100) 
    img  = models.ImageField(upload_to='media') # uploaded images will be stored in a folder. specify path here
    desc = models.TextField()
    price = models.IntegerField()
    offer = models.BooleanField(default=False)

    # #non-orm
    # id : int
    # name : str
    # desc : str
    # price : int 
    # offer : bool