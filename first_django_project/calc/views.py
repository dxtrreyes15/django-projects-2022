from django.shortcuts import render # for rendering templates
# from django.http import HttpResponse # for sending text response

# Create your views here.

def home(request): # req obj
    return render(request, 'calc/home.html', {'name' : 'Dexter'}) #can also pass dynamic data

def add(request):
    val1 = int(request.POST['num1'])
    val2 = int(request.POST['num2'])
    #operation
    res = val1 + val2
    return render(request, 'calc/add_result.html', {'result': res})