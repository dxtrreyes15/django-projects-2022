from operator import truediv
from django.shortcuts import render

from .models import Destination #import model
# Create your views here.

def index(request):
    
    # destination = Destination()
    # destination.name = "Philippines"
    # destination.desc = "Home of champions"
    # destination.img = "static/images/destination_1.jpg"
    # destination.price = 1000000
    # destination.offer = False

    # destination2 = Destination()
    # destination2.name = "Indonesia"
    # destination2.desc = "A country in Southeast Asia and Oceania between the Indian and Pacific oceans"
    # destination2.img = "static/images/destination_2.jpg"
    # destination2.price = 1
    # destination2.offer = True

    # destination3 = Destination()
    # destination3.name = "Singapore"
    # destination3.desc = "A sovereign island country and city-state in maritime Southeast Asia."
    # destination3.img = "/static/images/destination_3.jpg"
    # destination3.price = 20
    # destination3.offer = False
    
    # dest_list = [destination, destination2, destination3]

    dest = Destination.objects.all

    return render(request, 'travello/index.html', {'destination': dest})